from setuptools import setup


setup(
    name="oxal",
    use_scm_version=True,
    description="Convenience Python module to configure jpype for OpenXAL",
    url="https://gitlab.esss.lu.se/ics-infrastructure/py-openxal",
    author="Yngve Levinsen",
    author_email="yngve.levinsen@ess.eu",
    license="BSD",
    py_modules=["oxal"],
    install_requires=["JPype1>=0.7.0"],
    setup_requires=["setuptools_scm"],
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.7",
    ],
)
