import os

# As OpenXAL is not installed in the test environment, we have to
# set the OPENXAL_LIBRARY and OPENXAL_HOME environment variables before we import oxal
os.environ["OPENXAL_HOME"] = "/opt/OpenXAL"
os.environ["OPENXAL_LIBRARY"] = "/opt/OpenXAL/lib/library.jar"
import oxal
