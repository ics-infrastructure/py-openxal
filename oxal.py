"""
Convenience module to setup jpype for OpenXAL
"""
import glob
import os
import jpype
import atexit


class OxalInitializationError(Exception):
    pass


def openxal_library():
    """Return the path of the OpenXAL library (jar)

    Use the OPENXAL_LIBRARY environment if set.
    Otherwise try to find the library under openxal_home().
    Raise OxalInitializationError if not found.
    """

    if "OPENXAL_LIBRARY" in os.environ:
        return os.environ["OPENXAL_LIBRARY"]

    # We need to pick up a specific version if it is defined
    if "OPENXAL_VERSION" in os.environ:
        version = os.environ["OPENXAL_VERSION"]
    else:
        version = "*"

    openxal_path = openxal_home()

    # Installed by openxal_package.py:
    lib_path = os.path.join(openxal_path, "lib", "library.jar")
    if os.path.isfile(lib_path):
        library = lib_path
    else:
        # Installed "manually" using maven:
        try:
            for path in glob.glob(
                os.path.join(openxal_path, f"**/openxal.library-{version}.jar"),
                recursive=True,
            ):
                if path.split("-")[-1] != "sources.jar":
                    library = path
        except IndexError:
            raise OxalInitializationError(
                "Could not find OpenXAL library. You must set the OPENXAL_LIBRARY environment variable."
            )
    return os.path.normpath(library)


def openxal_version(library=None):
    """Return the OpenXAL version from the OpenXAL library jar file"""

    if "OPENXAL_VERSION" in os.environ:
        return os.environ["OPENXAL_VERSION"]

    # Library file holds the version in file name:
    if "library-" in library:
        return library.replace(".jar", "").split("library-")[-1]
    # A parent folder might hold the version in folder name:
    elif "openxal-" in library:
        for folder in library.split(os.sep):
            if folder.startswith("openxal-"):
                return folder[len("openxal-") :]
    # No way to figure out version from path remains?
    return None


def openxal_home():
    """Return path to OpenXAL home folder based on best effort

    Use the OPENXAL_HOME environment if set.
    Otherwise check the /opt/OpenXAL or ~/.m2/repository/org/xal directories.
    Raise OxalInitializationError if not found.
    """
    if "OPENXAL_HOME" in os.environ:
        return os.path.normpath(os.environ["OPENXAL_HOME"])
    else:
        for folder in [
            "/opt/OpenXAL",
            os.path.join(os.environ["HOME"], ".m2", "repository", "org", "xal"),
        ]:
            if os.path.isdir(folder):
                return os.path.normpath(folder)
    raise OxalInitializationError(
        "Could not find OpenXAL home folder. You must set the OPENXAL_HOME environment variable."
    )


OPENXAL_HOME = openxal_home()
OPENXAL_LIBRARY = openxal_library()
OPENXAL_VERSION = openxal_version(OPENXAL_LIBRARY)
# Add old variables for backward compatibility
OXAL_VERSION = OPENXAL_VERSION
JAVA_OpenXAL = OPENXAL_LIBRARY


def getDefaultPath():
    """Get the default model path

    :returns: The path to main.xal
    :rtype: str
    """
    dm = jpype.JClass("xal.smf.data.XMLDataManager")
    return dm.defaultPath()


def _shutdownOxal():
    """
    This function should be called
    before python exits.
    Registered to be called automatically,
    user do not need to call it manually.
    """
    cf = jpype.JClass("xal.ca.ChannelFactory")
    cf.disposeAll()


def startJVM(epics_server=None, java_flags="-ea", convertStrings=False):
    """Start the JVM with the proper configuration for OpenXAL

    :param epics_server: Force the EPICS_CA_ADDR_LIST variable
                         If undefined, use the existing EPICS_CA_ADDR_LIST env variable or localhost
    :param java_flags: Java flags to use

    :returns: True if started, False if it was already started
    :rtype: bool
    """
    java_classpath = "-Djava.class.path=" + OPENXAL_LIBRARY
    auto_addr = "false"
    if epics_server is None:
        try:
            epics_server = os.environ["EPICS_CA_ADDR_LIST"]
        except KeyError:
            epics_server = "127.0.0.1"
            auto_addr = "true"
    else:
        os.environ["EPICS_CA_ADDR_LIST"] = epics_server
    java_epics_ca_addr = "-Dcom.cosylab.epics.caj.CAJContext.addr_list={}".format(
        epics_server
    )
    java_epics_auto_addr = (
        "-Dcom.cosylab.epics.caj.CAJContext.auto_addr_list={}".format(auto_addr)
    )
    if jpype.isJVMStarted():
        return False
    jpype.startJVM(
        jpype.getDefaultJVMPath(),
        java_flags,
        java_epics_ca_addr,
        java_epics_auto_addr,
        java_classpath,
        convertStrings=convertStrings,
    )
    atexit.register(_shutdownOxal)

    return True


def loadAccelerator(path=None):
    """Load the accelerator model and return it.
    By default loads default accelerator as defined in OpenXAL config.

    :param path: Optionally provide path to model
    """
    dm = jpype.JClass("xal.smf.data.XMLDataManager")
    if path is None:
        return dm.loadDefaultAccelerator()
    else:
        return dm.acceleratorWithPath(path)
