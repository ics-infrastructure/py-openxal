py-openxal
==========

The *oxal* Python module is a convenience module to configure jpype for OpenXAL.
It exposes the jpype_ *startJVM* method with the correct parameters to use OpenXAL.


If the environment variable OPENXAL_LIBRARY is not set, the module will try to find the OpenXAL library (jar)
under /opt/OpenXAL or maven install path (~/.m2/repository/org/xal).
It will raise an exception if not found.

Installation
------------

For a quick installation of the latest release on your own system, run this command::

    pip install -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple -U oxal

The flag -U means upgrade if already installed.

Note that we encourage to work in a virtual environment::

    python3 -m venv venv
    source venv/bin/activate
    python3 -m pip install -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple oxal

To avoid having to pass the PyPI repository url in the command line, you can create the following `~/.pip/pip.conf` file::

    [global]
    index-url = https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple

The pypi-virtual repository aggregates packages from the local ics-pypi repository and the remote pypi-remote that serves
as a caching proxy for https://pypi.org.

If you use conda, you can also use a conda environment instead::

    conda config --set channel_alias https://artifactory.esss.lu.se/artifactory/api/conda
    conda create -c ics-conda-forge -c conda-forge -n <myenv> python=3.7 oxal
    conda activate <myenv>

Usage
-----

::

    import oxal

    oxal.startJVM()


If the EPICS_CA_ADDR_LIST environment variable is set, it will be passed to the JVM.
You can force a specific EPICS server when starting the JVM::

    import oxal

    epics_server = '10.x.x.x'
    oxal.startJVM(epics_server)


You can as well override the default JAVA flags (-ea) if required::

    import oxal

    oxal.startJVM(java_flags='-ea')


.. _jpype: https://github.com/originell/jpype
